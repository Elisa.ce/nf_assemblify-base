#!/usr/bin/env bash
###############################################################################
##                                                                           ##
##                  Converstion Sam to Bam and indexage                      ##
##                                                                           ##
###############################################################################

# var settings
args=("$@")
SAM_HIS=${args[0]}
CPUS=${args[1]}
LOGCMD=${args[2]}

BAM_OUT=$(basename ${SAM_HIS%.*}).bam

CMD1="samtools sort -o $BAM_OUT -@ $CPUS -m 2G -O bam -T tmp $SAM_HIS"
CMD2="samtools index $BAM_OUT"

# Keep command in log
echo $CMD1 > $LOGCMD
echo $CMD2 >> $LOGCMD

# Run Command
eval ${CMD1}
eval ${CMD2}
