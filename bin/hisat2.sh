#!/usr/bin/env bash
###############################################################################
##                                                                           ##
##               	Mapping read on genome     	                     ##
##                                                                           ##
###############################################################################

# var settings
args=("$@")
R1=${args[0]}
R2=${args[1]}
RED_FASTA=${args[2]} 
CPUS=${args[3]}
LOGCMD=${args[4]}

#Constant
INTRON=20000

SAM_OUT=$(basename ${R1%.fastq*}).sam
ID=${RED_FASTA%.*}

CMD="hisat2-build -p $CPUS $RED_FASTA ${RED_FASTA%.*} ; hisat2 -p $CPUS --no-unal -q -x $ID -1 ${R1} -2 ${R2} --max-intronlen $INTRON > $SAM_OUT"
       
# Keep command in log
echo $CMD > $LOGCMD

# Run Commands
eval $CMD

