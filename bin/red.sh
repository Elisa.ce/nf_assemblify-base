#!/usr/bin/env bash
###############################################################################
##                                                                           ##
##          	        Identify and mask repetitive regions                 ##
##                                                                           ##
###############################################################################

# var settings
args=("$@")
GENOME=${args[0]}
CPUS=${args[1]}
LOGCMD=${args[2]}

# Command to execute
CMD="Red -gnm . -msk masked -cor ${CPUS}"

# Keep command in log
echo ${CMD} > ${LOGCMD}

# Put resultat in a new folder
mkdir -p masked

# Execute command
eval ${CMD}
