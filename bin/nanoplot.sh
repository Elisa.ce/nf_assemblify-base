#!/usr/bin/env bash
###############################################################################
##                                                                           ##
##           Visualizations of sequencing data using nanoplot                ##
##                                                                           ##
###############################################################################

# var settings
args=("$@")
INPUT=${args[0]}
CPUS=${args[1]}
LOGCMD=${args[2]}

# Command to execute
CMD="NanoPlot -t ${CPUS} --plots kde dot hex --N50 \
         --title \"PacBio Hifi reads for $(basename ${INPUT%.hifi*})\" \
         --fastq ${INPUT} -o ."

# Keep command in log
echo ${CMD} > ${LOGCMD}

# Execute command
eval ${CMD}

