#!/usr/bin/env bash
###############################################################################
##                                                                           ##
##                    	Check assembly using BUSCO                           ##
##                                                                           ##
###############################################################################

# var settings
args=("$@")
ASSEMBLY_FILE=${args[0]}
DB_PATH=${args[1]}
DB_NAME=${args[2]}
NCPUS=${args[3]}
LOGCMD=${args[4]}

# Command to execute #ou res.busco
CMD="busco -c ${NCPUS} --offline -m genome -i ${ASSEMBLY_FILE} -o res.busco -l ${DB_PATH}/${DB_NAME}"

# Keep command in log
echo ${CMD} > ${LOGCMD}

# Execute command
eval ${CMD}



