process samtools {

    // label is used to find appropriate resources to use with this task.
    // see conf/resources.conf
    label 'midmem'

    // tag is used to display task name in Nextflow logs
    // Here we use input channel file name
    tag 'samtools'

    // Result files published in Netxtflow result directory.
    // See also 'output' directive, below.
    publishDir "${params.resultdir}/04b_samtools",      mode: 'copy', pattern: '*.bam'
    publishDir "${params.resultdir}/04b_samtools",      mode: 'copy', pattern: '*.bai'
    publishDir "${params.resultdir}/logs/samtools",     mode: 'copy', pattern: '*.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd", mode: 'copy', pattern: '*.cmd'

    // Workflow input stream. Since this stream can be a list of fastq files,
    input:
        each(sam_ch)

    // Workflow output stream
    output:
        path('*.bai')
        path('*.bam'), emit : bam_ch
        path('*.log')
        path('*.cmd')

    // Script to execute (located in bin/ sub-directory)
    script:
    """
    samtools.sh $sam_ch ${task.cpus} samtools_${sam_ch.baseName}.cmd >& samtools_${sam_ch.baseName}.log 2>&1
    """
}
