process hisat2 {

    // label is used to find appropriate resources to use with this task.
    // see conf/resources.conf
    label 'highmem'

    // tag is used to display task name in Nextflow logs
    // Here we use input channel file name
    tag 'hisat2'

    // Result files published in Netxtflow result directory.
    // See also 'output' directive, below.
    publishDir "${params.resultdir}/04a_hisat2",      mode: 'copy', pattern: '*.ht2'
    publishDir "${params.resultdir}/04a_hisat2",      mode: 'copy', pattern: '*.sam'
    publishDir "${params.resultdir}/logs/hisat2",     mode: 'copy', pattern: '*.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd", mode: 'copy', pattern: '*.cmd'

    // Workflow input stream. Since this stream can be a list of fastq files,
    input:
        path(fa_r1_ch)
	path(fa_r2_ch)
	path(red_ch)

    // Workflow output stream, sam_ch will be use with samtools, ht2_log is use for a multiqc 
    output:
        path('*.ht2')
        path('*.sam'), emit : sam_ch 
        path('*.log'), emit : ht2_log
        path('*.cmd')

    // Script to execute (located in bin/ sub-directory)
    script:
    """
    hisat2.sh $fa_r1_ch $fa_r2_ch $red_ch ${task.cpus} hisat2.cmd >& hisat2.log 2>&1
    """
}
