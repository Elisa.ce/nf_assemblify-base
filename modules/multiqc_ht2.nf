process multiqc_ht2 {

    label 'lowmem'

    tag "multiqc_ht2"

    publishDir "${params.resultdir}/04c_multiqc",	mode: 'copy', pattern: '*.html'
    publishDir "${params.resultdir}/logs/multiqc_hisat2",	mode: 'copy', pattern: 'multiqc*.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd",	mode: 'copy', pattern: 'multiqc*.cmd'

    input:
        path(ht2_log_ch)

    output:
        path("*.html")
        path("multiqc*.log")
        path("multiqc*.cmd")

    script:
    """
    multiqc.sh "." "multiqc_report" multiqc.cmd >& multiqc.log 2>&1
    """ 
}
