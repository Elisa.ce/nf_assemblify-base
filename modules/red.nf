process red {

    // label is used to find appropriate resources to use with this task.
    // see conf/resources.conf
    label 'midmem'

    // tag is used to display task name in Nextflow logs
    // Here we use input channel file name
    tag 'red'

    // Result files published in Netxtflow result directory.
    // See also 'output' directive, below.
    publishDir "${params.resultdir}/03_red",      mode: 'copy', pattern: 'masked/*.msk'
    publishDir "${params.resultdir}/logs/red",     mode: 'copy', pattern: 'red.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd", mode: 'copy', pattern: 'red.cmd'

    // Workflow input stream. Since this stream can be a list of fastq files,
    input:
        path(assembly_fa)

    // Workflow output stream
    output:
        path('masked/*.msk'), emit: red_ch
	path("red*.log")
	path("red*.cmd")

    // Script to execute (located in bin/ sub-directory)
    script:
    """
    red.sh $assembly_fa ${task.cpus} red_${assembly_fa.baseName}.cmd >& red_${assembly_fa.baseName}.log 2>&1
    """
}
